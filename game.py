from random import choice

kelime_listesi = [
    "kalem",
    "ananas",
    "elma",
    "akademik",
    "gozluk",
    "dans",
    "mola",
    "mesaj",
    "zincir",
    "bos",
    "zigon",
]

kelime = choice(kelime_listesi)
bilinen = []

can = 5

while can != 0:
    print("Kalan Hak: {} \n".format(can))
    for harf in kelime:
        if harf in bilinen:
            print(harf, end=" ")
        else:
            print("*", end=" ")
    harf = input("\nHarf giriniz: ")
    if harf in kelime:
        print("Harf Var")
        bilinen.append(harf)
    else:
        print("Harf Yok")
        can = can - 1

    if set(kelime) == set(bilinen):
        print("TEBRIKLER!")
        break
